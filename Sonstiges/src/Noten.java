import java.util.Scanner;

public class Noten {

	public static void main(String[] args) {
		{
			// Scannererstellung
			Scanner eingabe = new Scanner(System.in);
			
			//Begriffsdefinition mit Datentyp
			byte noteziffer;
		
			// Note als Zahl-Eingabe
			System.out.print("Gebe eine Note ein: ");
			noteziffer = eingabe.nextByte();
		
			// if-Bedingungen bzw. Notenausgaben
			if (noteziffer == 1) {
				System.out.println("Sehr gut");
			} else {

				if (noteziffer == 2) {
					System.out.println("Gut");
				} else {

					if (noteziffer == 3) {
						System.out.println("Befriedigend");
					} else {

						if (noteziffer == 4) {
							System.out.println("Ausreichend");
						} else {

							if (noteziffer == 5) {
								System.out.println("Mangelhaft");
							} else {
								
								if (noteziffer == 6) {
									System.out.println("Ungen�gend");
								} else {
									
									// Fehlerausgabe bei zu hoher Zahl
									System.out.println("Bitte gebe eine g�ltige Zahl zwischen 1 und 6 ein.");
								}
							}
						}
					}
				}
			}
		}
		}
	}