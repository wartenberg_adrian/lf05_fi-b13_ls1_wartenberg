import java.util.Scanner;

public class Monate {

	public static void main(String[] args) {
		{
			// Scannererstellung
			Scanner eingabe = new Scanner(System.in);

			// Begriffdefinition mit Datentyp
			byte monatszahl;

			System.out.print("Gebe eine Zahl als Dezimalwert ein: ");
			monatszahl = eingabe.nextByte();

			// if-Bedingungen bzw. Notenausgaben
			if (monatszahl == 1) {
				System.out.println("Januar");
			} else {

				if (monatszahl == 2) {
					System.out.println("Februar");
				} else {

					if (monatszahl == 3) {
						System.out.println("M�rz");
					} else {

						if (monatszahl == 4) {
							System.out.println("April");
						} else {

							if (monatszahl == 5) {
								System.out.println("Mai");
							} else {

								if (monatszahl == 6) {
									System.out.println("Juni");
								} else {

									if (monatszahl == 7) {
										System.out.println("Juli");
									} else {

										if (monatszahl == 8) {
											System.out.println("August");
										} else {

											if (monatszahl == 9) {
												System.out.println("September");
											} else {

												if (monatszahl == 10) {
													System.out.println("Oktober");
												} else {

													if (monatszahl == 11) {
														System.out.println("November");
													} else {

														if (monatszahl == 12) {
															System.out.println("Dezember");
														} else {

															// Fehlerausgabe bei ung�ltiger Zahl
															System.out.println("Bitte gebe eine g�ltige Zahl zwischen 1 und 12 ein!");
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}

			}
		}
	}
}
