import java.util.Scanner;

public class Fahrkartenautomat_FIB13_adrian_wartenberg {
	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		String option;
		Scanner eingabe = new Scanner(System.in);

		do {
			// Auslagerung der Kernstruktur des Fahrkartenautomats in Methoden
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.\n");

			// Frage danach ob der Vorgang wiederholt werden soll.
			System.out.println("M�chten Sie das Programm neu starten? (j/n)");
			option = eingabe.next();

		} while (option.contains("J") || option.contains("j"));

		System.out.println("\n=================");
		System.out.println("Programm beendet.");
		System.out.println("=================");

		eingabe.close();
	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		double gesamtbetrag = 0.0;
		boolean zahlen = false;				
		int ticketAuswahl;
		int ticketAnzahl = 0;		
		
		// Erste Aufgabe "Vorteil der 2 Array Methode:"
		// Beschreibung und Preis haben beide eine einfache Zugeh�rigkeit, zudem gibt es eine einheitliche
		// Stelle im an der man eventuelle �nderungen �ndern bzw. anpassen kann, ebenfalls funktioniert
		// der Code trotzdem, sollte man was an der Gr��e des Arrays ver�ndern.
		
		// Dritte Aufgabe "Vergleichen Sie die neue Implementierung"
		// Sollten Arrays ungleiche Gr��en haben, kommt es dennoch zu Fehlern.
		// Bei einer zu gro�en Ticketanzahl, kann es schnell un�bersichtlich werden, an welcher Stelle genau
		// die Preise eingef�gt werden m�ssen.
		
		
		
		// Ticketbeschreibung und Ticketpreise
		String[] ticketBeschreibung = {
				  "Einzelfahrschein Berlin AB"
				, "Einzelfahrschein Berlin BC" 
				, "Einzelfahrschein Berlin ABC"
				, "Kurzstrecke"
				, "Tageskarte Berlin AB"
				, "Tageskarte Berlin BC"
				, "Tageskarte Berlin ABC"
				, "Kleingruppen-Tageskarte Berlin AB"
				, "Kleingruppen-Tageskarte Berlin BC"
				, "Kleingruppen-Tageskarte Berlin ABC"};
		double[] ticketPreis = { 
				  2.90
				, 3.30
				, 3.60
				, 1.90
				, 8.60
				, 9.00
				, 9.60
				, 23.50
				, 24.30
				, 24.90};
		
		
		System.out.println("Bestellvorgang:");
		System.out.println("=========================");
		System.out.println("");

		do {
			System.out.println("\nW�hlen Sie ihr Ticket: ");

			// Ausgabe der Array Elemente und die option zum bezahlen
			for(int i = 0; i < ticketPreis.length; i++) {
				System.out.printf("  %s [%.2f EUR] (%d)\n", ticketBeschreibung[i], ticketPreis[i], i+1);
			}
			System.out.printf("\nBezahlen (%d)\n", ticketPreis.length+1);

			
			// Ticket auswahl
			System.out.print("\nTicket auswahl: ");
			ticketAuswahl = tastatur.nextByte();
			
			// Wenn man bezahlen ausw�hlt, bezahlen auf true setzen
			if (ticketAuswahl == ticketPreis.length+1) {
			zahlen = true;		
			}	
            
			// Wenn eine nicht vorhergesehende Eingabe get�tig wird, wird sie hier abgefangen und ein Fehler wird ausgegeben.
			while (ticketAuswahl <= 0 || ticketAuswahl > ticketPreis.length+1) {
			System.out.println(" Falsche Eingabe ");
			System.out.print("Eingabe: ");
			ticketAuswahl = tastatur.nextByte();
			}
			
			// Anzahl der Tickets
			if (zahlen != true) {
			System.out.print("Anzahl: ");
			ticketAnzahl = tastatur.nextByte();
			}
			
			// Es k�nnen maximal 0 bis 10 Tickets ausgegeben werden.
			while (ticketAnzahl > 10 || ticketAnzahl < 0) {
			System.out.println("Sie k�nnen maximal 10 Tickets kaufen, bitte w�hlen Sie eine Zahl zwischen 1 und 10.");
			System.out.print("Anzahl der Tickets: ");
			ticketAnzahl = tastatur.nextByte();
			}
			                
            
			// Berechnung der Tickets
			if (zahlen != true) {
				gesamtbetrag += ticketPreis[ticketAuswahl-1] * ticketAnzahl;
				System.out.printf("Zwischensumme: %.2f �\n", gesamtbetrag);
			}
			
			
		} while (zahlen != true);

		return gesamtbetrag;
	}

	public static double fahrkartenBezahlen(double zuZahlen) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMuenze;

		while (eingezahlterGesamtbetrag < zuZahlen) {
			System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlen - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2.00 Euro): ");
			eingeworfeneMuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}

		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			wartezeit(250);
		}
		System.out.println("\n\n");

	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		String einheit = "Euro";
		muenzeAusgeben(rueckgabebetrag, einheit);

	}

	// Wartezeit in Millisekunden
	public static void wartezeit(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// Ausgabe der M�nzen
	public static void muenzeAusgeben(double betrag, String einheit) {
		if (betrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f %s \n", betrag, einheit);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (betrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.printf("2 %s \n", einheit);
				betrag -= 2.0;
			}
			while (betrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.printf("1 %s \n", einheit);
				betrag -= 1.0;
			}
			while (betrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.printf("0.50 %s \n", einheit);
				betrag -= 0.5;
			}
			while (betrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.printf("0.20 %s \n", einheit);
				betrag -= 0.2;
			}
			while (betrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.printf("0.10 %s \n", einheit);
				betrag -= 0.1;
			}
			while (betrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.printf("0.05 %s \n", einheit);
				betrag -= 0.05;
			}
		}
	}
}