import java.util.Scanner;

public class OhmschesGesetz_FIB13_adrian_wartenberg {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);
		char angabe;
		double wert_u;
		double wert_r;
		double wert_i;
		double rechnung_u;


		System.out.print("Was soll berechnet werden?\n");
		System.out.print("Was davon soll ermittelt werden?: \n");
		System.out.print("\n'u' f�r Spannung");
		System.out.print("\n'r' f�r Widerstand");
		System.out.print("\n'i' f�r Stromst�rke.\n");
		
		angabe = tastatur.next().charAt(0);

		if (angabe != 'i' && angabe != 'r' && angabe != 'u') {
			System.out.print("\nFehler! Bitte starten Sie das Program neu!");
		}

		
		if (angabe == 'u') { // Spannungsberechnung
			System.out.print("Welchen Wert hat 'r' ?\n");
			wert_r = tastatur.nextDouble();
			
			System.out.print("Welchen Wert hat 'i' ?\n");
			wert_i = tastatur.nextDouble();
			rechnung_u = wert_r * wert_i;
			
			System.out.print("Die Spannung entspricht = " + rechnung_u + " V");
		}
		
		
		if (angabe == 'r') { // Widerstandsberechnung
			
			System.out.print("Welchen Wert hat 'u' ?\n");
			wert_u = tastatur.nextDouble();
			
			System.out.print("Welchen Wert hat 'i' ?\n");
			wert_i = tastatur.nextDouble();
			rechnung_u = wert_u / wert_i;
			
			System.out.print("Der Widerstand entspricht = " + rechnung_u + " ");
		}
		
		
		if (angabe == 'i') { // Stromst�rke Berechnung
			
			System.out.print("Welchen Wert hat 'u' ?\n");
			wert_u = tastatur.nextDouble();
			
			System.out.print("Welchen Wert hat 'i' ?\n");
			wert_r = tastatur.nextDouble();
			rechnung_u = wert_u / wert_r;
		
			System.out.print("Die Spannung entspricht = " + rechnung_u + " A");
		}
		tastatur.close();
	}// Ende der Main-Methode
}// Ende des Programmes.