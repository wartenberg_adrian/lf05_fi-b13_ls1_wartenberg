import java.util.Scanner;

public class BMI_FIB13_adrian_wartenberg {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		double Gewichtseingabe;
		double Gr��eneingabe;
		char Geschlechtsabfrage;

		System.out.print("Geben sie ihr Gewicht in kg an: ");
		Gewichtseingabe = eingabe.nextDouble();

		System.out.print("Geben sie ihre Gr��e in in Meter an: ");
		Gr��eneingabe = eingabe.nextDouble();

		System.out.print("Welches Geschlecht haben Sie?\n");
		System.out.print("Schreiben Sie �m� f�r M�nnlich \n");
		System.out.print("Schreiben Sie �w� f�r Weiblich\n");
		System.out.print(": ");

		Geschlechtsabfrage = eingabe.next().charAt(0);

		double bmiergebnis;
		double groesemalzwei;
		groesemalzwei = Gr��eneingabe * Gr��eneingabe;
		bmiergebnis = Gewichtseingabe / groesemalzwei;

		// M�nner

		if (bmiergebnis < 20 && Geschlechtsabfrage == 'm') {
			System.out.print("Sie sind Untergwichitig.");
			System.out.print("BMI: ");
			System.out.printf("%.2f", bmiergebnis);
		}

		if (bmiergebnis > 20 && bmiergebnis < 25 && Geschlechtsabfrage == 'm') {
			System.out.print("Sie sind im Normalgewicht.\n");
			System.out.print("BMI: ");
			System.out.printf("%.2f", bmiergebnis);
		}

		if (bmiergebnis > 25 && Geschlechtsabfrage == 'm') {
			System.out.print("Sie sind im �bergwicht.");
			System.out.print("BMI: ");
			System.out.printf("%.2f", bmiergebnis);
		} // Ende M�nner

		// Anfang Frauen
		if (bmiergebnis < 19 && Geschlechtsabfrage == 'w') {
			System.out.print("Sie sind Untergwichitig.");
			System.out.print("BMI: ");
			System.out.printf("%.2f", bmiergebnis);
		}

		if (bmiergebnis > 19 && bmiergebnis < 24 && Geschlechtsabfrage == 'w') {
			System.out.print("Sie sind im Normalgewicht.\n");
			System.out.print("BMI: ");
			System.out.printf("%.2f", bmiergebnis);
		}

		if (bmiergebnis > 24 && Geschlechtsabfrage == 'w') {
			System.out.print("Sie sind im �bergwicht.");
			System.out.print("BMI: ");
			System.out.printf("%.2f", bmiergebnis);
		} // Ende Frauen
		eingabe.close();	// Scanner "eingabe" wird geschlossen
	}// Ende der Main Methode

}// Ende des Programms