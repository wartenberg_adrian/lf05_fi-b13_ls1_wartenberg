import java.util.Scanner;

public class Taschenrechner_FIB13_adrian_wartenberg {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		double erstezahl;
		double zweitezahl;
		char rechenoperation;

		System.out.print("Gebe eine Zahl ein: ");
		erstezahl = eingabe.nextDouble();
		System.out.print("Welche Rechenoperation soll durchgef�hrt werden?\n");
		System.out.print("\n'+' f�r Addition");
		System.out.print("\n'-' f�r Subtraktion");
		System.out.print("\n'*' f�r Multiplikation");
		System.out.print("\n'/' f�r Division");
		System.out.print("\nBitte hier eingeben: ");
		rechenoperation = eingabe.next().charAt(0);
		System.out.print("Gebe eine zweite Zahl an: ");
		zweitezahl = eingabe.nextDouble();
		double ergebnis;

		// Addition
		if (rechenoperation == '+') {
			ergebnis = erstezahl + zweitezahl;
			System.out.print(erstezahl + " + " + zweitezahl + " = " + ergebnis);
		}

		// Subtraktion
		if (rechenoperation == '-') {
			ergebnis = erstezahl - zweitezahl;
			System.out.print(erstezahl + " - " + zweitezahl + " = " + ergebnis);
		}

		// Multiplikation
		if (rechenoperation == '*') {
			ergebnis = erstezahl * zweitezahl;
			System.out.print(erstezahl + " x " + zweitezahl + " = " + ergebnis);
		}

		// Division
		if (rechenoperation == '/') {
			ergebnis = erstezahl / zweitezahl;
			System.out.print(erstezahl + " / " + zweitezahl + " = " + ergebnis);
		}

		if (rechenoperation != '+' && rechenoperation != '-' && rechenoperation != '*' && rechenoperation != '/') {
			System.out.print("Fehler! Bitte starte das Programm neu!");
		}
		eingabe.close();
	}

}