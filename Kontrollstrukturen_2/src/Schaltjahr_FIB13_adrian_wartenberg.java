import java.util.Scanner;

public class Schaltjahr_FIB13_adrian_wartenberg {

	public static void main(String[] args) {

		Scanner jahreseingabe = new Scanner(System.in);
		double usereingabe;

		System.out.print("Gebe das Jahr ein, was du �berpr�fen m�chtest: ");
		usereingabe = jahreseingabe.nextDouble();

		// Sonderfall
		if (usereingabe == 1582) {
			System.out.print("Das Jahr 1582 war damals ein Schaltjahr.");
		}

		if (usereingabe % 4 == 0) {
			System.out.printf("\nDas Jahr ist ein Schaltjahr!");
			if (usereingabe % 100 == 0 && usereingabe % 400 != 0) {
				System.out.printf("\nDas Jahr ist ein Schaltjahr!!");
			}
		}

		if (usereingabe != 1582 && usereingabe % 4 > 0) {
			System.out.print("Das Jahr ist leider kein Schaltjahr.");
		}
		jahreseingabe.close();
	}

}