
import java.util.Scanner;

public class Million_FIB13_adrian_wartenberg {

	public static void main(String[] Args) {

		Scanner tastatureingabe = new Scanner(System.in);
		char eingabe = 'j';
		double Kapital, zinssatz;
		int i = 1;

		while (eingabe == 'j') {

			// Eingabe
			System.out.print("Wie viel Kapital soll angelegt werden?: ");
			Kapital = tastatureingabe.nextDouble();

			System.out.print("Bitte gebe den Zinssatz an: ");
			zinssatz = tastatureingabe.nextDouble();
			zinssatz = zinssatz / 100 + 1;

			// Verarbeitung
			while (Kapital < 1000000) {
				Kapital = Kapital * zinssatz;
				i++;
			}
			// Ausgabe
			System.out.println("\nNach " + i + " Jahren überschreitet das Kapital die 1.000.000 Euro\n");

			System.out.println("Soll das Programm noch mal gestartet werden? (j/n)");
			eingabe = tastatureingabe.next().charAt(0);
		}
		tastatureingabe.close();
	}
}