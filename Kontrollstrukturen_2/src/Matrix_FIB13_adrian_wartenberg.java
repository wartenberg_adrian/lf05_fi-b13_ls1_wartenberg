import java.util.Scanner;

public class Matrix_FIB13_adrian_wartenberg {

	public static void main(String[] args) {

		Scanner eingabe = new Scanner(System.in);

		System.out.print("Gebe eine Zahl zwischen 2 und 9 ein: ");
		int zahl = eingabe.nextInt();
		int i = 0;

		System.out.println("");

		while (i < 100) {

			int stelleEins = i / 10;
			int stelleZwei = i % 10;

			if (i % zahl == 0) // Soll die Teilbarkeit testen
				System.out.printf("%4s", "*");

			else if (stelleEins == zahl || stelleZwei == zahl) // Soll schauen ob die Zahl irgendwo vorkommt
				System.out.printf("%4s", "*");

			else if (stelleEins + stelleZwei == zahl) // Soll schauen ob die Zahl der Quersumme entspricht
				System.out.printf("%4s", "*");

			else
				System.out.printf("%4d", i);

			i++;

			if (i % 10 == 0 && i != 0) // Diese Zeile ist f�r den Umbruch am Zeilenende verantwortlich
				System.out.println();

		}
		eingabe.close();
	}
}
