import java.util.Scanner;

public class Quadrat_FIB13_adrian_wartenberg {

	public static void main(String[] args) {

		Scanner tastatureingabe = new Scanner(System.in);

		System.out.print("Was ist die L�nge des Quadrates?: \n");
		int size = tastatureingabe.nextInt() - 1;

		System.out.println("");

		for (int x = 0; x <= size; x++) {
			for (int y = 0; y <= size; y++) {
				if (x == 0 || x == size || y == 0 || y == size) {
					System.out.printf("%2s", "*");
				} else {
					System.out.print("  ");
				}
			}
			System.out.print("\n");
		}
		tastatureingabe.close();
	}

}
