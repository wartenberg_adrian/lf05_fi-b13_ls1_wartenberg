public class EinMalEins_FIB13_adrian_wartenberg {

	public static void main(String[] args) {

		int faktor1 = 1;
		int faktor2 = 1;
		int produkt;
		while (faktor2 < 11) {
			produkt = faktor1 * faktor2;
			System.out.print("\n" + produkt);
			faktor1 += 1;
			if (faktor1 == 11) {
				System.out.print("\n");
				faktor1 = 1;
				faktor2++;
			}
		} // Ende der While-Schleife
	}
}