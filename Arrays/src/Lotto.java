public class Lotto {

	public static void main(String[] args) {
		// Die Lottozahlen sind hier definiert 
		int[] lotto = { 3, 7, 12, 18, 37, 42};
		
		System.out.println("Lottozahlen:");
		
		// Hier findet die Lottoausgabe statt
		for (int i = 0; i < lotto.length; i++) {
			if (i == 0) {
				System.out.print("[ ");
			}
			
			System.out.print(lotto[i] + " ");
				
				if (i == lotto.length - 1) {
					System.out.print("]\n");
				}
			}
		
			 // Pr�fung
			if (enthalten(12, lotto) == true) {
				System.out.println("Die Zahl " + 12 + " ist in der Ziehung enthalten.");
			} else {
				System.out.println("Die Zahl " + 12 + " ist nicht in der Ziehung enthalten.");
			}
			
			if (enthalten(13, lotto) == true) {
				System.out.println("Die Zahl " + 13 + " ist in der Ziehung enthalten.");
			} else {
				System.out.println("Die Zahl " + 13 + " ist nicht in der Ziehung enthalten.");
			}
		}
		
		public static boolean enthalten(int x, int[] lotto) {
			boolean status = false;
			
			for (int i = 0; i < lotto.length; i++) {
				if (lotto[i] == x) {
					status = true;
				}
			}
			return status;
		}
	}