import java.util.Scanner;
public class Quadrieren {

	public static void main(String[] args) {
		int b = eingabe();
		System.out.println(quadriere(b));
	}
	
	// Eingabe
	public static int eingabe(){
		Scanner eingabe = new Scanner(System.in);
		System.out.println("Eingabe A:");
		int a = eingabe.nextInt();
		return a;
	}
	
	
	// Verarbeitung
	public static int quadriere(int x){
		return x * x;
		
	}

}