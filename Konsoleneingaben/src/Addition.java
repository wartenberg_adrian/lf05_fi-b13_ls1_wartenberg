import java.util.Scanner;	

	public class Addition {
	
		public static void main(String[] args) {
			
			Scanner myScanner = new Scanner(System.in);
			System.out.println("Geben sie eine Ganzzahl ein: ");
			
			int zahl1 = myScanner.nextInt();
			
			
			System.out.println("Bitte geben Sie eine zweite Ganzzahl ein: ");
			int zahl2 = myScanner.nextInt();
			
			
			int ergebnis = zahl1 + zahl2;
			
			System.out.println("\n\n\nDas Ergebnis der Addition lautet: ");
			System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnis);
			
			myScanner.close();
	
		}
	
	}
