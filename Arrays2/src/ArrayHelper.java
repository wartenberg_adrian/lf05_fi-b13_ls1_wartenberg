public class ArrayHelper {

	public static void main(String[] args) {
		// Ganzzahlen
				int[] zahlen = { 2, 3, 4, 5, 6, 7 };

				// 1 Aufgabe
				String ergebnisAufgabe1 = convertArrayToString(zahlen);
				System.out.println(ergebnisAufgabe1);
				
				// 2 Aufgabe
				int[] ergebnisAufgabe2 = arrayUmgekehrt(zahlen);
				System.out.println(java.util.Arrays.toString(ergebnisAufgabe2));
				
				// 3 Aufgabe
				int[] ergebnisAufgabe3 = arrayUmgekehrtNeu(zahlen);
				System.out.println(java.util.Arrays.toString(ergebnisAufgabe3));
				
				// 4 Aufgabe
				double[][] ergebnisAufgabe4 = temperaturTabelle(6);
			}

			// 1 Aufgabe
			public static String convertArrayToString(int[] zahlen) {
				String liste = "";

				for (int i = 0; i < zahlen.length; i++) {
					liste += zahlen[i];

					if (i == zahlen.length - 1) {
					} else {
						liste += ", ";
					}
				}
				return liste;
			}

			// 2 Aufgabe
			public static int[] arrayUmgekehrt(int[] zahlen) {
				int x, y;

				for (int i = 0; i < zahlen.length / 2; i++) {
					// Pos 1
					x = zahlen[i];
					// Pos 2
					y = zahlen[(zahlen.length - i) - 1];

					zahlen[i] = y;
					zahlen[zahlen.length - i - 1] = x;
				}
				return zahlen;
			}

			// 3 Aufgabe
			public static int[] arrayUmgekehrtNeu(int[] zahlen) {
				int[] umgekehrt = new int[zahlen.length];
				byte x = 0;

				for (int i = zahlen.length - 1; i >= 0; i--) {
					umgekehrt[x] = zahlen[i];
					x += 1;
				}
				return umgekehrt;
			}

			// 4 Aufgabe
			public static double[][] temperaturTabelle(int berechnung) {
				double[][] tabelle = new double[2][berechnung];
				tabelle[0][0] = 0.00;

				for (int i = 0; i < berechnung; i++) {
					if (i > 0) {
						tabelle[0][i] = tabelle[0][i - 1] + 10.0;
					}
					tabelle[1][i] = (5.0 / 9.0) * (tabelle[0][i] - 32);
				}

				tabelleErstellen(tabelle, berechnung);
				return tabelle;
			}

			public static void tabelleErstellen(double[][] tabelle, int berechnung) {
				System.out.printf("%-12s", "Fahrenheit");
				System.out.printf("|");
				System.out.printf("%10s\n", "Celsius");
				System.out.printf("-----------------------\n");
				
				for(int i = 0; i < berechnung;i++) {
					// Fahrenheit
					System.out.printf("%-12s", tabelle[0][i]);
					System.out.printf("|");
					
					// Celsius
					System.out.printf("%4s", "");
					System.out.printf("%.2f\n", tabelle[1][i]);
				}
			
			}
			
			// 5 Aufgabe
			public static void nmMatrix() {
				
				
			}

		}