
public class HelloWorld {

	public static void main(String[] args) {

		System.out.println("Guten Tag Adrian");
		System.out.println("Willkommen in der Veranstaltung strukturierte Programmierung.\n\n");
		System.out.println("Er sagte: \"Guten Tag!\"");
		
		//Variablen
		
		int alter = 21;
		String name = "Adrian";
		
		//Ausgabe
		
		System.out.println("Ich hei�e " + name + " und bin " + alter + " Jahre alt.\n\n");
		
		
		
		System.out.println("\n_________Formatierungstest_________");
		
		
		System.out.printf("\n\n Hello %s%n", "World");
		System.out.printf("Guten Tag%s%n", name);
		System.out.println("Gutan Tag");
		System.out.printf("Gutan Tag");
	}

}
