import java.util.Scanner; //Die Funktion Scanner wird importiert.
public class Aufgabe2 {

	public static void main(String[] args) {
		
		// Scanner wird benannt und erstellt
		Scanner tastatur = new Scanner(System.in);
	
		double zahl1; //Die "zahl1" wird hier deklariert, in dem Fall mit "double".
		double zahl2; //Die "zahl2" wird hier deklariert, in dem Fall mit "double".
		double ergebnis; //Das Ergebnis soll als eine Kommerzahl (Double) ausgegeben werden.
		
		System.out.println("Zahl 1 eingeben: "); //Eine Konsolenausgabe mit Text in Klammern.
		zahl1 = tastatur.nextDouble();			//Die Eingabe muss ein Double (Kommerzahl) sein. 
		System.out.println("Zahl 2 eingeben: "); //Eine Konsolenausgabe mit Text in Klammern.
		zahl2 = tastatur.nextDouble(); //Die Eingabe muss ein Double (Kommerzahl) sein.
		ergebnis = multiplizieren(zahl1, zahl2); //"ergebnis" wird mit "multiplizieren deklariert, es soll die zahl1 und zahl2 in Bezug nehmen
		System.out.println("Ergebnis:" + ergebnis);  // Hier wird das Ergebnis als Text ausgegeben.

	}
	
	public static double multiplizieren(double a, double b) { // Eine Methode wird erstellt und mit "multiplizieren" definiert.
		double ergebnis = a * b; // Hier findet die Multiplizieren statt
		return ergebnis; // Hier wird das ausgerechnete Ergebnis zur�ck zu "ergebnis" geliefert, um das Ergebnis ausgeben zu k�nnen.
	}
}