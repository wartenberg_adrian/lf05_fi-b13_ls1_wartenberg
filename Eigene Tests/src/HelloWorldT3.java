
public class HelloWorldT3 {

	public static void main(String[] args) {
		
		
		byte test1 = 127;								//"byte" hat einen Wertebereich von min -128 bis max +127
		short test2 = 128;								//"short" ein noch größeres Fassungsvermögen im Fassungsbereich, aufgrund dessen funkt. 32.767.
		boolean test3 = true;							//"boolean" einen Wertebereich von true oder false.
		double test4 = 2.123456789123456789;			//"double" Komma nur als "." Punkt verwenden, nicht als "," Komma. Programmierung orientiert
																	//sich an den Englischensprachraum.
		
		System.out.println(test1);
		System.out.println(test2);
		System.out.println(test3);
		System.out.println(test4);
		
	}

}
