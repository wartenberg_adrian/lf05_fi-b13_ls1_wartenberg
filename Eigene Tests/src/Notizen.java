
public class Notizen {

	public static void main(String[] args) {
		
		System.out.println("Das App-Kennwort f�r den Bitbucket-Push findest du in deiner KeePass-Datenbank.\n");
		System.out.println("Hinweis: Das App-Passwort dient nur zum Sichern in das Repository auf Bitbucket. "
				+ "Das Kennwort f�r den Bitbucket-Account, ist ein anderes.\n\n");
		System.out.println("Das Repository findest du unter: "
				+ "\n\"https://bitbucket.org/wartenberg_adrian/lf05_fi-b13_ls1_wartenberg/src/master/\".");
		

	}

}