
public class HelloWorldT2 {

	public static void main(String[] args) {

		int age = 20; // Initialisierung

		int age2; 	  // Deklaration
		age2 = 21; 	  // Zuweisung

		System.out.println(age);
		
		System.out.println(age2);

		age2 = 22;

		System.out.println(age2);

	}
}